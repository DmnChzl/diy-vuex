import { inject, computed } from 'vue';
import { CURRENT_IDX, INCREMENT_INDEX, DECREMENT_INDEX } from '@/StateProvider';

export default function useIndex() {
  const currentIdx = inject(CURRENT_IDX);
  const incrementIndex = inject(INCREMENT_INDEX);
  const decrementIndex = inject(DECREMENT_INDEX);

  return {
    currentIdx: computed(() => currentIdx.value),
    incrementIndex,
    decrementIndex
  };
}
