import { inject, computed } from 'vue';
import { FIELD_VALUES, SET_FIELD } from '@/StateProvider';

export default function useField(key) {
  const fieldValues = inject(FIELD_VALUES);
  const setField = inject(SET_FIELD);

  const setFieldByKey = key => value => setField(key, value);

  return [computed(() => fieldValues[key] || ''), setFieldByKey(key)];
}
